y;;; origami-ada-parser.el ---  -*- lexical-binding: t; -*- 

;; Copyright 2019 cnngimenez
;;
;; Author: cnngimenez
;; Version: $Id: origami-my-parsers.el,v 0.0 2019/12/11 23:01:25  Exp $
;; Keywords: 
;; X-URL: not distributed yet

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; 

;; Put this file into your load-path and the following into your ~/.emacs:
;;   (require 'origami-ada-parser)

;;; Code:

(require 'origami)
(require 'cl)
;; (require 'dash)


(defconst origami--ada-begin-words '("begin" "loop" "then")
  "Words that starts a new body"
  ) ;; defconst

(defconst origami--ada-end-words '("end")
  "Words that ends a body"
  ) ;; defconst

(defun origami--ada-ignore-comments ()
  "Ignore commens on the current word.

If before the current word there is a \"--\", jump to the next line ignoring
the comment."
  (when (string-match "--" (buffer-substring-no-properties
                           (point-at-bol) (point)))
    ;; this is a comment... ignore it all
    (next-line))
  ) ;; defun

(defun origami--ada-block-is-inside (child father)
  "Return true if the CHILD block is inside FATHER. 

CHILD and FATHER is a pair (begin end) where begin and end are point values."
  (and (< (car father) (car child))
       (< (cadr child) (cadr father)))
  ) ;; defun

(defun origami--ada-add-child (father child)
  "Add the child to the father."
  (if (nth 2 father)      
      (list (car father)
            (cadr father)
            (push child (nth 2 father)))
    (add-to-list 'father (list child) t)
    ) ;; if
  ) ;; defun


(defun origami--ada-detect-nesting (lst-points)
  "According to the inclusions in each (begin end) tuple in LST-POINTS,
add each children into the new list as (begin end (lst-children)).

These way it shows the nesting level of each begin-end block.

Example: 

    (origami--ada-detect-nesting ((314 356) (375 410) (230 428) (132 710))) 

Returns the following:

    ((132 710 (
       (230 428 (
         (375 410) (314 356))))))

Because (230 428) is between (132 710). (375 410) and (314 356) is inside 
(230 428). Also, (314 356) and (375 410) is not inside each other.
"

  (let ((lst-results '())
        )
    (while (not (seq-empty-p lst-points))

      ;; Take the first element
      (let* ((elt (pop lst-points))
             ;; Find a father for this element (another which ELT is inside).
             (pos-father (position-if
                          (lambda (father)
                            (origami--ada-block-is-inside elt father))
                          lst-points))
             (father (if pos-father
                         (nth pos-father lst-points)
                       nil))
             )

        ;; If a father is founded
        (if (null pos-father)
            ;; no... add it to the results
            (add-to-list 'lst-results elt)
          (progn
            ;; yes... add the ELT to its FATHER
            (setf (seq-elt lst-points pos-father)
                  (origami--ada-add-child father elt))
            ) ;; progn
          ) ;; if
        ) ;; let
      
      ) ;; while

      lst-results
    ) ;; let
  ) ;; defun


(defun origami--ada-find-blocks ()
  "Search for \"begin\" and \"end\" words in the current buffer.

Return their points position considering its nesting.

The strategy used is to search word by word. If a begin is founded, add the
char point where it is. If an end is founded, pop the point from the stack and
use it to create a (begin-point end-point) element."
  (let ((lst-begins '())
        (lst-results '())
        (lst-children '())
        )
    
    (while (right-word)

      (origami--ada-ignore-comments)
      
      (when (member (word-at-point) origami--ada-begin-words)
        ;; found a begin-word, add it to the list
        (add-to-list 'lst-begins (- (point)
                                    (length (word-at-point))))
        ) ;; when

      
      (when (member (word-at-point) origami--ada-end-words)
        ;; It's an end-word
        (let ((elt (list (pop lst-begins)
                         (point)))
              )
          
          ;; (when (= (length lst-begins) 0)                    
          (add-to-list 'lst-results elt)
          ;; ) ;; when
          ) ;; let

        ;; Ignore the "end loop" words
        (right-word)
        (if (equal "loop" (word-at-point))
            (right-word)
          (left-word))
        
        ) ;; when
      
      ) ;; while

    lst-results
    ) ;; let  
  ) ;; defun

(defun origami-ada-parser (create)
  "Find the begin-end blocks and call create on those positions." 
  (lambda (content)
    "Insert the given content and search for each begin-end block.
     Call create on all of them on the first and second nesting level. "
    (with-temp-buffer
      (insert content)
      (goto-char (point-min))

      ;; Only consider up to 2 begin-end nesting
      (mapcar (lambda (item)
                (funcall create (car item) (cadr item) 5

                         ;; second nesting
                         (mapcar (lambda (item)
                                   (funcall create (car item) (cadr item) 5 nil))
                                 (nth 2 item))
                         ))
                         
              (origami--ada-detect-nesting (reverse (origami--ada-find-blocks)))
              )
      )
    ) ;; lambda
  ) ;; defun

(defun origami--ada-test-create (elt1 elt2 elt3 elt4)
  "create function to pass to the `origami-ada-parser' function"
  (add-to-list 'test (list elt1 elt2 elt3 elt4)))

(defun origami--ada-test (buffer)
  "Test the `origami-ada-parser' against an adb buffer"
  (with-current-buffer buffer
    (setq test '())

    (goto-char (point-min))
    ;; (origami--ada-begin-parser)
    (funcall (origami-ada-parser 'origami--ada-test-create)
             (buffer-string))
    )
  ) ;; defun



(push '(ada-mode . origami-ada-parse) origami-parser-alist) 

(provide 'origami-ada-parser)
;;; origami-ada-parser.el ends here
